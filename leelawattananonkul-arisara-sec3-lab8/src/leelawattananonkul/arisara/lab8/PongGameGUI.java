package leelawattananonkul.arisara.lab8;

/**
 * PongGameGUI is a class which extends from SimpleGameWindow. 
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

public class PongGameGUI extends SimpleGameWindow {

	private static final long serialVersionUID = 1L;
	
	public PongGameGUI(String title) {
		super(title);
	}

	public static void main(String[] args) {
		PongPanel mainPanel = new PongPanel();
		PongGameGUI window = new PongGameGUI("Pong Game Window");
		window.setFrameFeatures();
		window.add(mainPanel);	
	}

}
