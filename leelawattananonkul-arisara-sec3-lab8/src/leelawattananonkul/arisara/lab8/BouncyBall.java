package leelawattananonkul.arisara.lab8;

/**
 * BouncyBall is a class which extends java class JPanel and implements Runnable interface.
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

import java.awt.*;
import javax.swing.JPanel;

public class BouncyBall extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	private int xVelo, yVelo; 
	private int width = 650, height = 500, r = 20;
	private float Xspeed = 5, Yspeed = 5;
	MovingBall ball;
	Thread running;
	
	public BouncyBall() {
		setBackground(Color.GREEN); // Set background to Green
		//Add a variable from MovingBall class
		ball = new MovingBall(width/2 - r, height/2 - r, r, xVelo, yVelo);
		
		running = new Thread(this);
		running.start();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball); //Fill the ball with yellow color
	}
	
	@Override
	public void run() {
		while(true) {
			ballUpdate(); 
			repaint();
			this.getToolkit().sync();
			
			try {
				Thread.sleep(20);
			}
			catch(InterruptedException ex) {
				System.err.println(ex.getStackTrace());
			}
			
		}
		
	}
	
	public void ballUpdate() {
		ball.x += Xspeed;
		ball.y += Yspeed;
		
		if (ball.x < 0) { // turn to left 
            Xspeed = - Xspeed; // Reflect along normal
            ball.x = 0; // Re-position the ball at the edge
         } else if (ball.x > width - 2*r) { //turn to right
            Xspeed = - Xspeed; 
            ball.x = width - 2*r; 
         }
         
         if (ball.y < 0) { // turn to top
        	 	Yspeed = -Yspeed; 
        	 	ball.y = 0; 
         } else if (ball.y > height - 3*r) { // turn to bottom
            Yspeed = -Yspeed;
            ball.y = height - 3*r;
         }
   
	}
	
}
