package leelawattananonkul.arisara.lab8;

/**
 * WrapAroundBall is a class which extends java class JPanel and implements Runnable interface.
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

import java.awt.*;
import javax.swing.*;

public class WrapAroundBall extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;
	private int xVelo, yVelo; 
	private int width = 650, height = 500, r = 20;
	private float velo = (float) 2;
	MovingBall ball1, ball2, ball3, ball4, ball5;
	Thread running;
	
	public WrapAroundBall() {
		setBackground(Color.BLUE); // Set Background to Blue.
		// Add five variables from MovingBall class
		ball1 = new MovingBall(width/4-2*r, 0 - 6*r, r, xVelo, yVelo); 
		ball2 = new MovingBall(0 - 6*r, height/4, r, xVelo, yVelo); 
		ball3 = new MovingBall(width/4, 0 - 6*r, r, xVelo, yVelo); 
		ball4 = new MovingBall(width - 300, height + 6*r, r, xVelo, yVelo); 
		ball5 = new MovingBall(width + 6*r, height - 200, r, xVelo, yVelo); 

		running = new Thread(this);
		running.start();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g;
		// Set color for every ball
		g2d.setColor(Color.YELLOW);
		g2d.fill(ball1);
		g2d.setColor(Color.RED);
		g2d.fill(ball2);
		g2d.setColor(Color.GREEN);
		g2d.fill(ball3);
		g2d.setColor(Color.BLACK);
		g2d.fill(ball4);
		g2d.setColor(Color.WHITE);
		g2d.fill(ball5);
		
	}
	
	@Override
	public void run() {
		while(true) {
			ballUpdate();
			repaint();
			this.getToolkit().sync();
			
			try {
				Thread.sleep(20);
			}
			catch(InterruptedException ex) {
				System.err.println(ex.getStackTrace());	
			}
			
		}
		
	}
	
	public void ballUpdate() {
		//Ball1 is yellow and set that's moving from top to bottom
		ball1.x += 0;
		ball1.y += velo;
		if (ball1.y > height) {
			// Re-position the ball at the edge
			ball1.x = width/4 - 2*r;
			ball1.y = 0 - 4*r; 
		}
		
		//Ball2 is red and set that's moving from left to right
		ball2.x += velo;
		ball2.y += 0;
		if (ball2.x > width) {
			// Re-position the ball at the edge
			ball2.x = 0 - 4*r;
			ball2.y = height/8;
		}
		
		//Ball3 is Green and set that's moving in the diagonal direction
		ball3.x += velo/2;
		ball3.y += velo;
		if (ball3.x > width || ball3.y > height) {
			// Re-position the ball at the edge
			ball3.x = ball3.x;
			ball3.y = 0 - 4*r;
		}
		if (ball3.x > width) {
			// Re-position the ball at the edge
			ball3.x = 0;
			ball3.y = 0 - 4*r;
		}
		
		
		//Ball4 is Black and set that's moving from bottom to top
		ball4.x += 0;
		ball4.y += -velo;
		if (ball4.y < 0 - 2*r) {
			// Re-position the ball at the edge
			ball4.x = width-300;
			ball4.y = height + 4*r;
		}

		//Ball5 is white and set that's moving right to left
		ball5.x += -velo;
		ball5.y += 0;
		if (ball5.x <= 0 - 2*r) {
			// Re-position the ball at the edge
			ball5.x = width + 2*r;
			ball5.y = height - 200;
		}
	}

}
