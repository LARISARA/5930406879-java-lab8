package leelawattananonkul.arisara.lab8;

/**
 * Ball is a class which extends java class Ellipse2D.Double. Class Ball has the following properties: 
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

import java.awt.geom.Ellipse2D;

public class Ball extends Ellipse2D.Double {
	
	private static final long serialVersionUID = 1L;
	private int r;

	public Ball(int x, int y, int r) {
		super(x, y, r*2, r*2);
		this.r = r;
	}

	public int getR() {
		return r;
	}

	public void setR(int r) {
		this.r = r;
	}
	
}
