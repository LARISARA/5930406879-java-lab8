package leelawattananonkul.arisara.lab8;

/**
 * PongPaddle is a class which extends java class Rectangle2D.Double. 
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

import java.awt.geom.Rectangle2D;

public class PongPaddle extends Rectangle2D.Double {

	private static final long serialVersionUID = 1L;
	
	public PongPaddle(int x, int y, int width, int height) {
		super(x, y, width, height);
	}	

}
