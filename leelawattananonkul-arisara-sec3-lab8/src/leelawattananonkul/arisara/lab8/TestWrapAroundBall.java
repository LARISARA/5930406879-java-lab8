package leelawattananonkul.arisara.lab8;

/**
 * TestWrapAroundBall is a class which extends from SimpleGameWindow. 
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

public class TestWrapAroundBall extends SimpleGameWindow {

	private static final long serialVersionUID = -5897468282291075818L;
	
	public TestWrapAroundBall(String title) {
		super(title);
	}
	
	public static void main(String[] args) {
		WrapAroundBall wrapballPanel = new WrapAroundBall();
		TestWrapAroundBall window = new TestWrapAroundBall("Test Wrap Around Ball");
		window.setFrameFeatures();
		window.add(wrapballPanel);
	}

}

