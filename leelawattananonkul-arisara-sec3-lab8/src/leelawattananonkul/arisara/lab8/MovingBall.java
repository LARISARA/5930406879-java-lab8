package leelawattananonkul.arisara.lab8;

/**
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

public class MovingBall extends Ball {

	private static final long serialVersionUID = 1L;
	private int xVelo, yVelo;
	
	public MovingBall(int x, int y, int r, int xVelo, int yVelo) {
		super(x, y, r);
		this.xVelo = xVelo;
		this.yVelo = yVelo;
	}
	
	public int getxVelo() {
		return xVelo;
	}

	public void setxVelo(int xVelo) {
		this.xVelo = xVelo;
	}

	public int getyVelo() {
		return yVelo;
	}

	public void setyVelo(int yVelo) {
		this.yVelo = yVelo;
	}

}
