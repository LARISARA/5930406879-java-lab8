package leelawattananonkul.arisara.lab8;

/**
 * PongPanel is a class which extends java class JPanel. PongPanel has the following properties:
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

import java.awt.*;
import java.awt.geom.Line2D;
import javax.swing.JPanel;

public class PongPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	private int score1, score2;
	private int width = 650, height = 500;
	static int rad = 20, padWidth = 15, padHeight = 80;
	private Ball ball;
	private PongPaddle paddle1, paddle2;
	
	public PongPanel() {
		this.setBackground(Color.BLACK); // Set background to Black
		this.score1 = 0;
		this.score2 = 0;
		// Add variables from Ball class and PongPaddle class
		ball = new Ball((width/2) - 20, (height/2) - rad, rad); //
		paddle1 = new PongPaddle(width - 15, (height/2) - 2*rad, padWidth, padHeight);
		paddle2 = new PongPaddle(0, (height/2) - 2*rad, padWidth, padHeight);
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Font pongFont = new Font("Serif", Font.BOLD, 48); // Setting font
		
		Graphics2D g2d = (Graphics2D) g;
		Line2D lineLeft = new Line2D.Float(padWidth, 0, padWidth, height); 
		Line2D lineRight = new Line2D.Float(width - 15, 0, width - 15, height);
		Line2D lineMiddle = new Line2D.Float(width/2, 0, width/2, height);
	
		g2d.setFont(pongFont);
		g2d.setColor(Color.WHITE); 
		
		g2d.draw(lineLeft);
		g2d.draw(lineRight);
		g2d.draw(lineMiddle);
		
		g2d.fill(paddle1);
		g2d.fill(paddle2);
		g2d.fill(ball);
		
		//Setting scores
		int scorePosition = 100;
		g2d.drawString(Integer.toString(score1), width/4, scorePosition);
		g2d.drawString(Integer.toString(score2), (width*3)/4, scorePosition);
	}

}
