package leelawattananonkul.arisara.lab8;

/**
 * TestBouncyBall is a class which extends from SimpleGameWindow. 
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

public class TestBouncyBall extends SimpleGameWindow {

	private static final long serialVersionUID = 1L;

	public TestBouncyBall(String title) {
		super(title);
	}
	
	public static void main(String[] args) {
		BouncyBall BouncyBallPanel = new BouncyBall();
		TestBouncyBall window = new TestBouncyBall("Test Bouncy Ball");
		window.setFrameFeatures();
		window.add(BouncyBallPanel);
	}
	
}
