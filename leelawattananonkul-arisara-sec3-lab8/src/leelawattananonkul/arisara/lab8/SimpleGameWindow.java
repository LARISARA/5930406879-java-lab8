package leelawattananonkul.arisara.lab8;

/**
 * SimpleGameWindow is a class which extends java class JFrame. SimpleGameWindow has the following properties:
 * @author  Arisara Leelawattananonkul
 * @593040687-9
 * @since   2018-03-19
 */

import java.awt.*;
import javax.swing.*;

public class SimpleGameWindow extends JFrame {

	private static final long serialVersionUID = 1L;

	public SimpleGameWindow(String title) {
		super(title);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	protected static void createAndShowGUI() {
		SimpleGameWindow window = new SimpleGameWindow("Pong Game Window");
		window.setFrameFeatures();
	}

	protected void setFrameFeatures() {
		pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		int xSize = 650;
		int ySize = 500;
		int width = this.getWidth();
		int height = this.getHeight();
		int x = (dim.width-width)/2 - (xSize/2);
		int y = (dim.height-height)/2 - (ySize/2);
		setSize(xSize, ySize);
		setLocation(x, y);
		setDefaultLookAndFeelDecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

}
